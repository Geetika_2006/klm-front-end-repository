import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { FlightDetailsComponent } from './flight-details/flight-details.component';
import { MetricsComponent } from './metrics/metrics.component';
import { HomePageComponent } from './home-page/home-page.component';

const routes: Routes = [
  {
    path: '',
    children: [{
      path: 'flightsearch', component: FlightDetailsComponent
    },
    {
      path: 'metrics', component: MetricsComponent
    }
,{
   path:'home', component: HomePageComponent
},

 { path: '**', redirectTo: '/home', pathMatch: 'full' }

  ]
}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
