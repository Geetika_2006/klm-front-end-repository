import { Component, OnInit } from '@angular/core';
import { SharedService } from '../services/app.search.service';

@Component({
  selector: 'app-metrics',
  templateUrl: './metrics.component.html',
  styleUrls: ['./metrics.component.css']
})
export class MetricsComponent implements OnInit {
  public metricList= [];     //[{"httpCode":"200","count":1}];
  constructor(private sharedService:SharedService) { 
    this.sharedService=sharedService;
  }

  ngOnInit() {
    this.sharedService.getMetrics().subscribe(response => {
      this.metricList = response;
    });
  }


  
}

