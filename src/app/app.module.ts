import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { RouterModule, Routes } from '@angular/router';

import { AppComponent } from './app.component';
import { FlightDetailsComponent } from './flight-details/flight-details.component';
import { AppRoutingModule } from './app-routing.module';
import { AppData } from './services/app.data.service';
import { SharedService } from './services/app.search.service';
import { HttpModule } from '@angular/http';
import { MetricsComponent } from './metrics/metrics.component';
import { HomePageComponent } from './home-page/home-page.component';

@NgModule({
  declarations: [
    AppComponent,
    FlightDetailsComponent,
    MetricsComponent,
    HomePageComponent
  ],
  imports: [
    BrowserModule,FormsModule,RouterModule,AppRoutingModule,HttpModule
  ],
  providers: [AppData,SharedService],
  bootstrap: [AppComponent]
})
export class AppModule { }
