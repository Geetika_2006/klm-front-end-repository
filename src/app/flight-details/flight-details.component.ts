import { Component, OnInit, ElementRef, OnChanges, SimpleChanges, Input } from '@angular/core';
import { AppData } from '../services/app.data.service';
import { SharedService } from '../services/app.search.service';

@Component({
  selector: 'app-flight-details',
  templateUrl: './flight-details.component.html',
  styleUrls: ['./flight-details.component.css'],
  providers:[SharedService],
})
export class FlightDetailsComponent implements OnInit
   {

  fareList=[];
  origin: string;
  destination: string;
  countries=[];

@Input() query:string;
  
  
  ngOnInit() {
   this.sharedService.populateAirports().subscribe(response => {
      this.countries = response;
       console.log(response);
    },
     error =>
        {
        console.log(error);
         });
  }


    //public query = '';
    public filteredList = [];
    public destinationList = [];
    public elementRef;
    showTable=false;
    constructor(myElement: ElementRef, private appData:AppData, private sharedService:SharedService) {
        this.elementRef = myElement;
       this.sharedService =sharedService;
    }

    filter() {
    if (this.query !== ""){
        this.filteredList = this.countries.filter(function(el){
            return el.toLowerCase().indexOf(this.query.toLowerCase()) > -1;
        }.bind(this));
        
    }else{
        this.filteredList = [];
    }
}
 

  filterDestination() {
    if (this.query !== ""){
        this.destinationList = this.countries.filter(function(el){
            return el.toLowerCase().indexOf(this.destination.toLowerCase()) > -1;
        }.bind(this));
    }else{
        this.destinationList = [];
    }
}

select(item){
    this.query = item;
    this.filteredList = [];
    this.appData.origin=this.query;
    //console.log(this.query);
}

selectDestination(destination){
    this.destination = destination;
    this.destinationList = [];
    this.appData.destination=this.destination;
    console.log(destination);
     console.log(this.appData.origin);
}

handleClick(event){
   var clickedComponent = event.target;
   var inside = false;
   do {
       if (clickedComponent === this.elementRef.nativeElement) {
           inside = true;
       }
      clickedComponent = clickedComponent.parentNode;
   } while (clickedComponent);
    if(!inside){
        this.filteredList = [];
    }
}

   getFare(){
    this.showTable=true;
    var origin =this.appData.origin;
    var destination =this.appData.destination;
    console.log("destination"+destination);
    console.log('origin!!'+origin);
      this.sharedService.getFare(origin,destination).subscribe(response => {
      this.fareList = response;
       console.log(response);
    },
     error =>
        {
        console.log(error);
         });


        }


    
                
      }


