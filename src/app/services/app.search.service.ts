import { Injectable, AfterViewInit } from '@angular/core';
import {Http, RequestOptions} from '@angular/http'; 
import { Observable } from 'rxjs';
//import { Observable } from 'rxjs/Rx';
//import { Observable } from "rxjs/Rx";
//import 'rxjs/add/operator/map'
import { map } from 'rxjs/operators';
//import 'rxjs/Rx';
@Injectable()
export class SharedService 

    {
    constructor(private http: Http) {

    }

    
   
   getFare(origin:string,destination:string){
    const url =  'http://localhost:9000/travel/fares/' + origin +'/'+destination;
    const fareJson ='/assets/fares.json';
     return this.http.get(fareJson).pipe(map(response => response.json()));
    }

 getAutosuggestion(input:string){
    const url =  'http://localhost:9000/travel/airports' + input;
     return this.http.get(url).pipe(map(response => response.json()));
    }

    getMetrics(){
    const url =  'http://localhost:9000/travel/statusMetric';
    const statusJson = '/assets/statusMetric.json'
    return this.http.get(statusJson).pipe(map(response => response.json()));
    }

  populateAirports(){
     const url =  'http://localhost:9000/travel/airports';
     const airportJson='/assets/airport.json';
     return this.http.get(airportJson).pipe(map(response => response.json()));
  }

              }          


