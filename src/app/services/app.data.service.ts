import { Injectable, AfterViewInit } from '@angular/core';
import { Http } from '@angular/http';
import { Observable } from 'rxjs';


@Injectable()
export class AppData {

constructor(){
    
}
    public origin: any;
    public destination: any;

    get getOrigin(): any {
        return this.origin;
    }

    setOrigin(value: any) {
        this.origin = value;
    }

    getDestination(): any {
        return this.destination;
    }

    setDestination(value: any) {
        this.destination = value
    }
}
